#!/bin/sh

set -eu

CROSS_COMPILE="${CROSS_COMPILE:-}"

TEST_DIR=$(mktemp -d)

test_failed=false

cleanup()
{
    if [ "$(dirname "${TEST_DIR}")" != "/tmp" ]; then
        exit 1
    fi
    rm -rf "${TEST_DIR}"
}

trap cleanup EXIT

check_compiler()
{
cat <<-EOT > "${TEST_DIR}/compiler_test.c"
	int main(void)
	{
	    return 0;
	}
EOT

    "${CROSS_COMPILE}gcc" \
        -o "${TEST_DIR}/compiler_test" \
        -c "${TEST_DIR}/compiler_test.c" \
        -Wall -Werror || test_failed=true

    "${CROSS_COMPILE}objdump" -dS "${TEST_DIR}/compiler_test" 1> /dev/null || test_failed=true

    printf "compiler support: "
    if "${test_failed}"; then
        echo "error"
    else
        echo "ok"
    fi
}

check_compiler

if "${test_failed}"; then
   echo "ERROR: Missing preconditions, cannot continue."
   exit 1
fi

echo "All Ok"

exit 0
