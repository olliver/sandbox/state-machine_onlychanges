CC=gcc
CFLAGS=-Wall -Werror -O0 -g --static
LDFLAGS=


include src/Makefile

all: state-machine

state-machine: src/state-machine.o
	${CC} -o state-machine src/state-machine.o ${CFLAGS} ${LDFLAGS}

.PHONY: clean

clean:
	rm -f src/*.o state-machine
